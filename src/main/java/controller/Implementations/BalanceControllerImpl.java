package controller.Implementations;

import controller.Interfaces.BalanceController;
import model.domain.bank.DefaultTransaction;
import view.Interfaces.BalanceView;
import view.Interfaces.MoreFunctionsView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * controller che gestisce la balance view
 */
public class BalanceControllerImpl implements BalanceController {

	static BalanceView balanceView;
	
	/**
	 * costruttore
	 * @param balanceView la view
	 */
	public BalanceControllerImpl(BalanceView balanceView){
		BalanceControllerImpl.balanceView=balanceView;
		balanceView.addBackListener(new BackListener());
		showBalance();
	}
	
	/**
	 * listener che fa tornare alla pagina precedente
	 */
	public class BackListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			balanceView.dispose();
			new MoreFunctionsControllerImpl(new MoreFunctionsView(balanceView.getRank()));
		}
		
	}
	
	public void showBalance(){
		int balance=0;
		//salvo tutti i movimenti passati in una lista
		List<DefaultTransaction>list=AddMovementControllerImpl.InsertListener.getMovements();
		//li ciclo e calcolo il bilancio
		for(DefaultTransaction m:list){
				switch(m.getType()){
			case WITHDRAW: balance-=m.getMoneyAmount();
				break;
			case DEPOSIT : balance+=m.getMoneyAmount();
				break;
			}
		}
		//stampo il bilancio
		balanceView.setLabel(String.valueOf(balance));
		//creo una tabella con tutti i movimenti
		String[]vet={"+ : -","amount","desc","data"};
		String[][]mat=new String[list.size()][vet.length];
		for(int i=0;i<list.size();i++){
				mat[i][0]=String.valueOf(list.get(i).getType());
				mat[i][1]=String.valueOf(list.get(i).getMoneyAmount());
				mat[i][2]=list.get(i).getDescription();
				mat[i][3]=list.get(i).getIssuingDate().toString();
		}
		JTable table=new JTable(mat,vet);
		balanceView.createTable(table);
		
	}
}
