package controller.Implementations;

import model.domain.person.prisoner.Prisoner;
import model.domain.visit.PrisonVisit;
import view.Interfaces.AddVisitorsView;
import view.Interfaces.MoreFunctionsView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * controller della addVisitorsView
 */
public class AddVisitorsControllerImpl {
	
	static AddVisitorsView visitorsView;
	
	/**
	 * costruttore
	 * @param view la view
	 */
	public AddVisitorsControllerImpl(AddVisitorsView view)
	{
		AddVisitorsControllerImpl.visitorsView=view;
		visitorsView.addBackListener(new BackListener());
		visitorsView.addInsertVisitorListener(new InsertListener());
	}
	
	/**
	 * listener che fa tornare alla pagina precedente
	 */
	public class BackListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			visitorsView.dispose();
			new MoreFunctionsControllerImpl(new MoreFunctionsView(visitorsView.getRank()));
		}
		
	}
	
	/**
	 * listener che gestisce l'inserimento di visitatori
	 */
	public static class InsertListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {

			//recupero la lista dei visitatori e la salvo in una lista
			List<PrisonVisit> visits = null;
			try {
				visits = getVisitors();
			} catch (ClassNotFoundException | IOException e) {
				e.printStackTrace();
			}
			//salvo il visitatore inserito nella view
			PrisonVisit vi = visitorsView.getVisit();
			//controllo che non ci siano errori
			try {
				if(vi.getPrisoner().getName().length()<2||vi.getPrisoner().getSurname().length()<2 || !checkPrisonerID(vi))
					visitorsView.displayErrorMessage("Devi inserire un nome, un cognome e un prigioniero esistente");
				else{
					//inserisco il visitatore nella lista
					visits.add(vi);
					visitorsView.displayErrorMessage("Visitatore inserito");
				}
				} catch (ClassNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
				//salvo la lista aggiornata
				setVisitors(visits);

			}
		
		/**
		 * ritorna la lista dei visitatori
		 * @return lista dei visitatori
		 * @throws IOException
		 * @throws ClassNotFoundException
		 */
		public static List<PrisonVisit> getVisitors() throws IOException, ClassNotFoundException
		{
			File f = new File("res/Visitors.txt");
			//se il file è vuoto ritorno una lista vuota
			if(f.length()!=0){
				FileInputStream fi = new FileInputStream(f);
				ObjectInputStream oi = new ObjectInputStream(fi);
				
				List<PrisonVisit> visit = new ArrayList<>();
				//salvo il cotenuto del file in una lista
				try{
					while(true){
						PrisonVisit s = (PrisonVisit) oi.readObject();
						visit.add(s);
					}
				}catch(EOFException eofe){}
				
				fi.close();
				oi.close();
				return visit;
			}
				return new ArrayList<PrisonVisit>();
		}
		}
	
	/**
	 * controlla se l'id del prigioniero inserita è corretta
	 * @param v visitatore
	 * @return true se l'id è corretto
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public static boolean checkPrisonerID(PrisonVisit v) throws ClassNotFoundException, IOException{
		List<Prisoner> lista = MainControllerImpl.getCurrentPrisoners();
		boolean found = false;
		//ciclo tutti i prigionieri
		for(Prisoner p : lista)
		{
			//se gli id conicidono restituisco true
			if(p.getId() == v.getPrisoner().getId())
				{
					found=true;
					return found;
				}
			else
				continue;
		}
		return false;
	}
	
	/**
	 * salva la lista dei visitatori aggiornata
	 * @param visits lista dei visitatori
	 */
	public static void setVisitors(List<PrisonVisit> visits){
		File f = new File("res/Visitors.txt");
		FileOutputStream fo = null;
		try {
			fo = new FileOutputStream(f);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		ObjectOutputStream os = null;
		try {
			os = new ObjectOutputStream(fo);
		} catch (IOException e) {
			e.printStackTrace();
		}
		//salvo su file la lista aggiornata di visitatori
		for(PrisonVisit g1 : visits){
			try {
				os.writeObject(g1);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			os.close();
			fo.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}



