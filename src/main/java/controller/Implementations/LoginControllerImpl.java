package controller.Implementations;

import model.domain.person.Policeman;
import view.Interfaces.LoginView;
import view.Interfaces.MainView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * controller della login view
 */
public class LoginControllerImpl {
	
	LoginView loginView;

	/**
	 * costruttore
	 * @param loginView la view
	 */
	public LoginControllerImpl(LoginView loginView){
		this.loginView=loginView;
		loginView.addLoginListener(new LoginListener());
		loginView.displayErrorMessage("accedere con i profili: \n id:3 , password:qwerty \n id:2 , password:asdasd ");
	}
	
	/**
	 * listener che si occupa di effettuare il login
	 */
	public class LoginListener implements ActionListener{
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
						
			List<Policeman> policemen = null;
			try {
				//salvo la lista delle guardie nel file
				policemen = getGuards();
			} catch (ClassNotFoundException | IOException e) {
				e.printStackTrace();
			}
			boolean isInside=false;

			//controllo che non ci siano errori
			if(loginView.getUsername().isEmpty() || loginView.getPassword().isEmpty()){
				loginView.displayErrorMessage("Devi inserire username e password");
			}
			else{
				for (Policeman g : policemen){
					if(loginView.getUsername().equals(String.valueOf(g.getAccount().getUsername())) && loginView.getPassword().equals(g.getAccount().getPassword())){
						//effettuo il login
						isInside=true;
						loginView.displayErrorMessage("Benvenuto Utente "+ loginView.getUsername());	
						loginView.dispose();
						new MainControllerImpl(new MainView(g.getRank()));
					}
				}
				if(isInside==false){
					loginView.displayErrorMessage("Combinazione username/password non corretta");
				}
				isInside = false;
			}
		}
		
	}
	
	/**
	 * metodo che restutuisce la lista di guardie salvata su file
	 * @return lista di guardie
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static List<Policeman> getGuards() throws IOException, ClassNotFoundException{
		File f = new File("res/GuardieUserPass.txt");
		//se il file è vuoto restituisco un file vuoto
		if(f.length()!=0){
			FileInputStream fi = new FileInputStream(f);
			ObjectInputStream oi = new ObjectInputStream(fi);
			
			List<Policeman> policemen = new ArrayList<>();
			
			try{
				while(true){
					//salvo ogni guardia in una lista
					Policeman s = (Policeman) oi.readObject();
					policemen.add(s);
				}
			}catch(EOFException eofe){}
			
			fi.close();
			oi.close();
			return policemen;
		}
		return new ArrayList<Policeman>();
		
	}
}
