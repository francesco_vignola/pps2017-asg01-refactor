package controller.Implementations;

import controller.Interfaces.InsertGuardController;
import model.domain.person.Policeman;
import view.Interfaces.InsertGuardView;
import view.Interfaces.SupervisorFunctionsView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.List;

/**
 * controller che gestisce la insert guard view
 */
public class InsertGuardControllerImpl implements InsertGuardController {

	static InsertGuardView insertGuardView;
	
	/**
	 * costruttore
	 * @param insertGuardView la view
	 */
	public InsertGuardControllerImpl(InsertGuardView insertGuardView){
		InsertGuardControllerImpl.insertGuardView=insertGuardView;
		insertGuardView.addBackListener(new BackListener());
		insertGuardView.addInsertListener(new InsertListener());
	}
	
	/**
	 * listener che fa tornare alla pagina precedente
	 */
	public class BackListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			insertGuardView.dispose();
			new SupervisorControllerImpl(new SupervisorFunctionsView(insertGuardView.getRank()));
		}
		
	}
	
	public void insertGuard(){
		List<Policeman> policemen = null;
		//salvo le guardie in una lista
		try {
			policemen = LoginControllerImpl.getGuards();
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
		//recupero la guardia inserita nella view
		Policeman g = insertGuardView.getGuard();
		boolean contains=false;
		//controllo che non ci siano errori
		for (Policeman g1 : policemen){
			if(g1.getId()==g.getId()){
				insertGuardView.displayErrorMessage("ID già usato");
				contains=true;
			}
		}
		if(isSomethingEmpty(g)){
			insertGuardView.displayErrorMessage("Completa tutti i campi correttamente");
			contains=true;
		}
		if(contains==false){
			//inserisco la guardia e salvo la lista aggiornata
			policemen.add(g);
			setGuards(policemen);
			insertGuardView.displayErrorMessage("Guardia inserita");

		}
	}
	
	/**
	 * listener che si occupa di inserire una guardia
	 */
	public class InsertListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			insertGuard();
		}
		
	}
	
	public boolean isSomethingEmpty(Policeman g){
		if(g.getName().equals("")||g.getSurname().equals("")||g.getRank()<1||g.getRank()>3||g.getId()<0||g.getAccount().getPassword().length()<6)
			return true;
		return false;
	}
	
	/**
	 * salva le guardie 
	 * @param policemen lista di guardie
	 */
	public static void setGuards(List<Policeman> policemen){

		File f = new File("res/GuardieUserPass.txt");
		FileOutputStream fo = null;
		try {
			fo = new FileOutputStream(f);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		ObjectOutputStream os = null;
		try {
			os = new ObjectOutputStream(fo);
		} catch (IOException e) {
			e.printStackTrace();
		}
		//ciclo tutte le guardie nella lista e li scrivo su file
		for(Policeman g1 : policemen){
			try {
				os.writeObject(g1);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			os.close();
			fo.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
