package controller.Interfaces;

import model.domain.person.Policeman;

public interface InsertGuardController {

	/**
	 * inserisce una nuova guardia
	 */
	public void insertGuard();
	
	/**
	 * controlla se la guardia è stata implementata correttamente
	 * @param g la guardia
	 * @return true se la guardia è stata implementata male
	 */
	public boolean isSomethingEmpty(Policeman g);
}
