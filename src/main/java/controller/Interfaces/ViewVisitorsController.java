package controller.Interfaces;

import javax.swing.*;

public interface ViewVisitorsController {

	/**
	 * crea una tabella contenente i visitatori
	 * @return la tabella
	 */
	JTable getTable();
}
