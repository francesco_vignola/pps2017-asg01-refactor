package model.persistence.repository;

import model.persistence.Identifiable;
import model.persistence.criteria.Criteria;

import java.util.Optional;
import java.util.stream.Stream;

public interface Repository<T extends Identifiable> {

    void add(T element);

    void remove(T element);

    <R extends Criteria> Optional<T> find(R criteria);

    <R extends Criteria> Stream<T> filter(R criteria);

    void clear();

    default void addAll(Stream<T> elements) {
        elements.forEach(this::add);
    }

    default void removeAll(Stream<T> elements) {
        elements.forEach(this::remove);
    }
}
