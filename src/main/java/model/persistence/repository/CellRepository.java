package model.persistence.repository;

import model.domain.holding_cell.DefaultHoldingCell;
import model.persistence.criteria.Criteria;
import model.persistence.criteria.FileCriteria;
import model.persistence.datamanager.FileReader;
import model.persistence.datamanager.FileWriter;
import model.persistence.datamanager.Reader;
import model.persistence.datamanager.Writer;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CellRepository implements Repository<DefaultHoldingCell> {
    private final static String fileName = "HoldingCell.txt";
    private final static String path =
            Paths.get(System.getProperty("user.dir"), "res", fileName).toString();

    private Reader<DefaultHoldingCell> reader = new FileReader<>(path);
    private Writer<DefaultHoldingCell> writer = new FileWriter<>(path);

    @Override
    public void add(DefaultHoldingCell element) {
        Stream<DefaultHoldingCell> allCells = reader.readAll();
        writer.write(Stream
                .concat(Stream.of(element), allCells)
                .collect(Collectors.toList()));
    }

    @Override
    public void remove(DefaultHoldingCell element) {
        Stream<DefaultHoldingCell> allCells = reader.readAll();
        writer.write(allCells
                .filter(holdingCell -> holdingCell.getId() != element.getId())
                .collect(Collectors.toList()));
    }

    @SuppressWarnings("unchecked")
    @Override
    public <R extends Criteria> Stream<DefaultHoldingCell> filter(R criteria) {
        List<DefaultHoldingCell> allCells = new ArrayList<>();
        if (criteria instanceof FileCriteria) {
            FileCriteria fc = (FileCriteria) criteria;
            Stream<DefaultHoldingCell> st = reader.readAll().filter(fc);
            allCells.addAll(st.collect(Collectors.toList()));
        }
        return allCells.stream();
    }

    @Override
    public <R extends Criteria> Optional<DefaultHoldingCell> find(R criteria) {
        return filter(criteria).findFirst();
    }

    @Override
    public void clear() {
        writer.write(Collections.emptyList());
    }

}
