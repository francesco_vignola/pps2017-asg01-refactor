package model.persistence.criteria;

import model.domain.holding_cell.HoldingCell;

public class HoldingCellByCapacity implements FileCriteria<HoldingCell> {
    private final int capacity;

    public HoldingCellByCapacity(int capacity) {
        this.capacity = capacity;
    }

    @Override
    public boolean test(HoldingCell holdingCell) {
        return holdingCell.getCapacity() == this.capacity;
    }
}
