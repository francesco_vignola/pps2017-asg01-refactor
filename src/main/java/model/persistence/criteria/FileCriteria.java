package model.persistence.criteria;


import model.persistence.Identifiable;

import java.util.function.Predicate;

public interface FileCriteria<T extends Identifiable> extends Predicate<T>, Criteria {}
