package model.persistence.criteria;

import model.domain.holding_cell.HoldingCell;

public class HoldingCellById implements FileCriteria<HoldingCell> {

    private long id;

    public HoldingCellById(long id) {
        this.id = id;
    }

    @Override
    public boolean test(HoldingCell holdingCell) {
        return holdingCell.getId() == id;
    }
}
