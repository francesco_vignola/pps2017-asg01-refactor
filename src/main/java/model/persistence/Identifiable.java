package model.persistence;

import java.io.Serializable;

public interface Identifiable extends Serializable {
    /**
     * Method to get the identification number of an entity.
     * @return the identification number.
     */
    long getId();
}
