package model.persistence.datamanager;

import org.apache.commons.io.FileUtils;
import org.jfree.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class FileReader<T extends Serializable> implements Reader<T> {

    private final File file;

    public FileReader(String fileName) {
        this.file = new File(fileName);
    }

    @Override
    public Stream<T> readAll() {
        List<T> objects = new ArrayList<>();
        try (FileInputStream fileInputStream = FileUtils.openInputStream(file)) {
            try (ObjectInputStream reader = new ObjectInputStream(fileInputStream)) {
                List<T> list = (List<T>) reader.readObject();
                objects.addAll(list);
            }
        } catch (Exception e) {
            Log.error(e.getMessage());
        }

        return objects.stream();
    }
}
