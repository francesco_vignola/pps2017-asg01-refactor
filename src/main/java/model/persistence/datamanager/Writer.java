package model.persistence.datamanager;

import java.io.Serializable;
import java.util.Collection;

public interface Writer<T extends Serializable> {
    void write(Collection<T> element);
}
