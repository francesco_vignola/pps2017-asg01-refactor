package model.persistence.datamanager;

import java.io.Serializable;
import java.util.stream.Stream;

public interface Reader<T extends Serializable> {
    Stream<T> readAll();
}
