package model.persistence.datamanager;

import org.apache.commons.io.FileUtils;
import org.jfree.util.Log;

import java.io.*;
import java.util.Collection;

public class FileWriter<T extends Serializable> implements Writer<T> {

    private final File file;

    public FileWriter(String fileName) {
        this.file = new File(fileName);
    }

    @Override
    public void write(Collection<T> elements) {
        try (FileOutputStream fileOutputStream = FileUtils.openOutputStream(file)) {
            try (ObjectOutputStream writer = new ObjectOutputStream(fileOutputStream)) {
                writer.writeObject(elements);
                writer.flush();
            }
        } catch (IOException e) {
            Log.error(e.getMessage());
        }
    }
}
