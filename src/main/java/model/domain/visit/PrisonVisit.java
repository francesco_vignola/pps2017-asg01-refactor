package model.domain.visit;

import model.domain.person.Person;
import model.domain.person.prisoner.Prisoner;

public interface PrisonVisit {

    /**
     * Method to get the person who visited the prisoner.
     * @return the person who visited the prisoner.
     */
    Person getVisitor();

    /**
     * Method to get the prisoner visited.
     * @return the prisoner visited.
     */
    Prisoner getPrisoner();
}
