package model.domain.visit;

import model.domain.person.Person;
import model.domain.person.prisoner.Prisoner;

/**
 * Model the concept of prison visit from a person.
 */
public class DefaultPrisonVisit implements PrisonVisit {

    private final Person visitor;
    private final Prisoner prisoner;

    public DefaultPrisonVisit(Person visitor, Prisoner prisoner) {
        this.visitor = visitor;
        this.prisoner = prisoner;
    }

    /**
     * Method to get the person who visited the prisoner.
     *
     * @return the person who visited the prisoner.
     */
    @Override
    public Person getVisitor() {
        return this.visitor;
    }

    /**
     * Method to get the prisoner visited.
     *
     * @return the prisoner visited.
     */
    @Override
    public Prisoner getPrisoner() {
        return this.prisoner;
    }
}
