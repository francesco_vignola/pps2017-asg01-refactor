package model.domain.crime;

/**
 * Model that represent a crime.
 */
public interface Crime {
    /**
     * Method to get the name of a crime.
     * @return name of a crime.
     */
    String getName();
}
