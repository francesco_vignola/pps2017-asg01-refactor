package model.domain.crime;

public enum DefaultCrime implements Crime {

    AGAINST_ANIMALS("Crimes against animals"),
    ASSOCIATIVES("Associative crimes"),
    BLASPHEMY_SACRILEGE("Blasphemy and sacrilege"),
    ECONOMIC_FINANCIAL("Economic and financial crimes"),
    FALSE_TESTIMONY("False testimony crime"),
    MILITARY("Military crimes"),
    AGAINST_PROPERTY("Crimes against property"),
    AGAINST_PERSON("Crimes against person"),
    ITALIAN_LEGA_SYSTEM("Crimes in the Italian legal system"),
    TAX("Tax crimes"),
    DRUG_TRAFFICKING("Drug trafficking crime"),
    FRAUD("Crimes of fraud");

    private final String name;

    DefaultCrime(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }
}
