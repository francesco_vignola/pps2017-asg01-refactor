package model.domain.person;

import model.domain.account.Account;
import model.persistence.Identifiable;

/**
 * Model the concept of the Policeman
 */
public interface Policeman extends Person, Identifiable {

    /**
     * Method to get the police rank.
     * @return the police rank.
     */
    int getRank();

    /**
     * Method to set the police rank.
     * @param rank police rank.
     */
    void setRank(int rank);

    /**
     * Method to get the telephone number.
     * @return the telephone number.
     */
    String getTelephone();

    /**
     * Method to set the telephone number.
     * @param telephone telephone number.
     */
    void setTelephone(String telephone);

    /**
     * Method to get the account details of policeman.
     * @return the account.
     */
    Account getAccount();
}
