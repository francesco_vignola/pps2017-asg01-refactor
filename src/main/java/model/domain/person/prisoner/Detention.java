package model.domain.person.prisoner;

import model.domain.crime.Crime;
import model.domain.holding_cell.HoldingCell;

import java.util.Date;
import java.util.Set;

public interface Detention {
    /**
     * Method to get the holding cell.
     *
     * @return the holding cell.
     */
    HoldingCell getCell();

    /**
     * Method to get all crimes as a reason for detention.
     *
     * @return all crimes.
     */
    Set<Crime> getCrimes();

    /**
     * Method to set one or more crimes as a reason for detention.
     *
     * @param crimes one or more crimes.
     */
    void setCrimes(Crime... crimes);

    /**
     * Method to get the start date of detention.
     *
     * @return the date of beginning detention.
     */
    Date getStartDetention();

    /**
     * Method to set the start data of detention.
     *
     * @param startDetention the start date of detention.
     */
    void setStartDetention(Date startDetention);

    /**
     * Method to get the end date of detention.
     *
     * @return end date of detention.
     */
    Date getEndDetention();

    /**
     * Method to set the end date of detention.
     *
     * @param endDetention the end date of detention.
     */
    void setEndDetention(Date endDetention);
}
