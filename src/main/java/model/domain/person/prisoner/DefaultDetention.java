package model.domain.person.prisoner;

import model.domain.crime.Crime;
import model.domain.holding_cell.HoldingCell;

import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class DefaultDetention implements Detention {

    private final HoldingCell cell;
    private Set<Crime> crimes = new HashSet<>();
    private Date startDetention, endDetention;

    public DefaultDetention(HoldingCell cell, Date startDetention, Date endDetention) {
        this.cell = cell;
        this.setStartDetention(startDetention);
        this.setEndDetention(endDetention);
    }

    /**
     * Method to get the holding cell.
     *
     * @return the holding cell.
     */
    @Override
    public HoldingCell getCell() {
        return this.cell;
    }

    @Override
    public Set<Crime> getCrimes() {
        return this.crimes;
    }

    /**
     * Method to set one or more crimes as a reason for detention.
     *
     * @param crimes one or more crimes.
     */
    @Override
    public void setCrimes(Crime... crimes) {
        Collections.addAll(this.crimes, crimes);
    }

    /**
     * Method to get the start date of detention.
     *
     * @return the date of beginning detention.
     */
    @Override
    public Date getStartDetention() {
        return this.startDetention;
    }

    /**
     * Method to set the start data of detention.
     *
     * @param startDetention the start date of detention.
     */
    @Override
    public void setStartDetention(Date startDetention) {
        this.startDetention = startDetention;
    }

    /**
     * Method to get the end date of detention.
     *
     * @return end date of detention.
     */
    @Override
    public Date getEndDetention() {
        return endDetention;
    }

    /**
     * Method to set the end date of detention.
     *
     * @param endDetention the end date of detention.
     */
    @Override
    public void setEndDetention(Date endDetention) {
        this.endDetention = endDetention;
    }
}
