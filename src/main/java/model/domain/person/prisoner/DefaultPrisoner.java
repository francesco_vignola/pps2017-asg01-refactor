package model.domain.person.prisoner;

import model.domain.crime.Crime;
import model.domain.person.DefaultPerson;

import java.util.*;

import static java.util.Comparator.comparing;

/**
 * Class that implement the concept of a prisoner.
 */
public class DefaultPrisoner extends DefaultPerson implements Prisoner {

    private static final long serialVersionUID = -3204660779285410481L;

    private final long id;
    private SortedSet<Detention> detentions =
            new TreeSet<>(comparing(Detention::getStartDetention));

    /**
     * Constructor to build a prisoner.
     *
     * @param name     the first name.
     * @param surname  the last name.
     * @param birthday the birthday date
     * @param id       the identification number.
     */
    public DefaultPrisoner(String name, String surname, Date birthday,
                           long id, Detention detention) {
        super(name, surname, birthday);
        this.id = id;
        this.addDetention(detention);
    }

    /**
     * Method to get the identification number of an entity.
     *
     * @return the identification number.
     */
    @Override
    public long getId() {
        return this.id;
    }

    /**
     * Method to get the last detention.
     *
     * @return last detention.
     */
    @Override
    public Detention getLastDetention() {
        return this.detentions.last();
    }

    /**
     * Method to get all the detentions.
     *
     * @return alla detentions.
     */
    @Override
    public SortedSet<Detention> getAllDetentions() {
        return this.detentions;
    }

    /**
     * Method to add a detention.
     *
     * @param detention
     */
    @Override
    public void addDetention(Detention detention) {
        this.detentions.add(detention);
    }

    /**
     * Method to get the all crimes of the prisoner.
     *
     * @return the crimes of prisoner.
     */
    @Override
    public Set<Crime> getCrimes() {
        return this.detentions.stream()
                .map(Detention::getCrimes)
                .reduce(Collections.emptySet(), (crimes, crimes2) -> {
                    crimes.addAll(crimes2);
                    return crimes;
                });
    }


    @Override
    public String toString() {
        return "Prisoner [ id: " + getId() + ", name: " + getName() + ", surname: " + getSurname()
                + ", crimes: [\n" + getCrimes().stream().map(crime -> crime.getName() + "\n") + "] ]";
    }

}
