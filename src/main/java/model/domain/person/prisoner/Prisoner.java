package model.domain.person.prisoner;

import model.domain.crime.Crime;
import model.domain.person.Person;
import model.persistence.Identifiable;

import java.util.Set;

/**
 * Model the concept of the prisoner
 */
public interface Prisoner extends Person, Identifiable {

    /**
     * Method to get the last detention.
     *
     * @return last detention.
     */
    Detention getLastDetention();

    /**
     * Method to get all the detentions.
     *
     * @return alla detentions.
     */
    Set<Detention> getAllDetentions();

    /**
     * Method to add a detention.
     *
     * @param detention
     */
    void addDetention(Detention detention);

    /**
     * Method to get the all crimes of the prisoner.
     *
     * @return the crimes of prisoner.
     */
    Set<Crime> getCrimes();
}