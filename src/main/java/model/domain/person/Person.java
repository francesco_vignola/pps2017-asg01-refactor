package model.domain.person;

import java.util.Date;

/**
 * Model the concept of person with your personal data.
 */
public interface Person {

    /**
     * Method to get the first name of the person.
     * @return the first name.
     */
    String getName();

    /**
     * Method to set the person's name.
     * @param name the first name.
     */
    void setName(String name);

    /**
     * Method to get the last name of the person.
     * @return the last name.
     */
    String getSurname();

    /**
     * Method to set the last name of the person.
     * @param surname the last name.
     */
    void setSurname(String surname);

    /**
     * Method to get the person's birthday date.
     * @return the birthday date.
     */
    Date getBirthday();

    /**
     * Method to set the person's birthday date.
     * @param birthday the birthday date
     */
    void setBirthday(Date birthday);
}
