package model.domain.person;

import java.util.Date;

/**
 * Class the implement the concept of a general person.
 */
public class DefaultPerson implements Person {

    private String name;
    private String surname;
    private Date birthday;

    /**
     * Constructor to build a person.
     *
     * @param name     name of the person.
     * @param surname  surname of the person.
     * @param birthday birthday date of the person.
     */
    public DefaultPerson(final String name, final String surname, final Date birthday) {
        super();
        this.setName(name);
        this.setSurname(surname);
        this.setBirthday(birthday);
    }

    /**
     * Method to get the first name of the person.
     *
     * @return the first name.
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Method to set the person's name.
     *
     * @param name the first name.
     */
    @Override
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Method to get the last name of the person.
     *
     * @return the last name.
     */
    @Override
    public String getSurname() {
        return surname;
    }

    /**
     * Method to set the last name of the person.
     *
     * @param surname the last name.
     */
    @Override
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * Method to get the person's birthday date.
     *
     * @return the birthday date.
     */
    @Override
    public Date getBirthday() {
        return birthday;
    }

    /**
     * Method to set the person's birthday date.
     *
     * @param birthday the birthday date
     */
    @Override
    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    @Override
    public String toString() {
        return "Person [ name: " + this.getName()
                + ", surname: " + this.getSurname()
                + ", birthday: " + this.getBirthday() + " ]";
    }


}
