package model.domain.person;

import model.domain.account.Account;
import model.domain.account.DefaultAccount;

import java.util.Date;

/**
 * Class that implement the concept of policeman of the prison.
 */
public class DefaultPoliceman extends DefaultPerson implements Policeman {

    private static final long serialVersionUID = 3975704240795357459L;

    private final long id;
    private int rank;
    private String telephone;
    private final Account account;

    /**
     * Constructor to build a policeman.
     *
     * @param name      first name.
     * @param surname   last name.
     * @param birthday  birthday date.
     * @param rank      police rank.
     * @param telephone telephone number.
     * @param id        identification number.
     * @param password  password.
     */
    private DefaultPoliceman(final String name, final String surname, final Date birthday,
                            final int rank, final String telephone, final long id, final String password) {
        super(name, surname, birthday);
        this.id = id;
        this.setRank(rank);
        this.setTelephone(telephone);
        this.account = new DefaultAccount(String.valueOf(this.getId()), password);
    }

    /**
     * Method to get the identification number of an entity.
     *
     * @return the identification number.
     */
    @Override
    public long getId() {
        return id;
    }

    /**
     * Method to get the police rank.
     *
     * @return the police rank.
     */
    @Override
    public int getRank() {
        return rank;
    }

    /**
     * Method to set the police rank.
     *
     * @param rank police rank.
     */
    @Override
    public void setRank(int rank) {
        this.rank = rank;
    }

    /**
     * Method to get the telephone number.
     *
     * @return the telephone number.
     */
    @Override
    public String getTelephone() {
        return telephone;
    }

    /**
     * Method to set the telephone number.
     *
     * @param telephone telephone number.
     */
    @Override
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * Method to get the account details of policeman.
     * @return the account.
     */
    @Override
    public Account getAccount() {
        return this.account;
    }

    @Override
    public String toString() {
        return "Policeman [ id: " + id + ", rank: " + rank
                + ", telephone number:" + telephone + " ]";
    }

    public static class PolicemanBuilder {

        private Date birthday;
        private long id = Integer.MIN_VALUE;
        private int rank = Integer.MIN_VALUE;
        private String name = "", surname = "", telephone = "", password = "";

        public PolicemanBuilder setId(long id) {
            this.id = id;
            return this;
        }

        public PolicemanBuilder setRank(int rank) {
            this.rank = rank;
            return this;
        }

        public PolicemanBuilder setBirthday(Date birthday) {
            this.birthday = birthday;
            return this;
        }

        public PolicemanBuilder setName(String name) {
            this.name = name;
            return this;
        }

        public PolicemanBuilder setSurname(String surname) {
            this.surname = surname;
            return this;
        }

        public PolicemanBuilder setTelephone(String telephone) {
            this.telephone = telephone;
            return this;
        }

        public PolicemanBuilder setPassword(String password) {
            this.password = password;
            return this;
        }

        public Policeman build() {
            if(this.id == Integer.MIN_VALUE || this.rank == Integer.MIN_VALUE
                    || this.name.trim().isEmpty() || this.password.trim().isEmpty()
                    || this.surname.trim().isEmpty() || this.telephone.trim().isEmpty()
                    || this.birthday == null) {
                throw new IllegalArgumentException("All fields are mandatory");
            }
            return new DefaultPoliceman(name, surname, birthday, rank, telephone, id, password);
        }
    }

}
