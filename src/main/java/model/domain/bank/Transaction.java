package model.domain.bank;

import model.persistence.Identifiable;

import java.util.Date;

/**
 * Model the concept of the money transaction.
 */
public interface Transaction extends Identifiable {

    /**
     * Method to get the date of the transaction.
     * @return the date of the transaction.
     */
    Date getIssuingDate();

    /**
     * Method to get the type of the transaction.
     * @return the type of the transaction.
     */
    TransactionType getType();

    /**
     * Method to get the amount of the transaction.
     * @return amount of the transaction
     */
    double getMoneyAmount();

    /**
     * Method to set the amount of the transaction.
     * @param amount of the transaction.
     */
    void setMoneyAmount(double amount);

    /**
     * Method to get the reason of the transaction.
     * @return the reason of the transaction.
     */
    String getDescription();

    /**
     * Method to set the description of the transaction.
     * @param description the reason of the transaction.
     */
    void setDescription(String description);
}
