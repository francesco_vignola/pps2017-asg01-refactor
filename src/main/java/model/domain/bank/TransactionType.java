package model.domain.bank;

public enum TransactionType {
    DEPOSIT("Deposit"),
    WITHDRAW("Withdraw");

    private final String name;

    TransactionType(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Transaction type: " + this.name;
    }
}
