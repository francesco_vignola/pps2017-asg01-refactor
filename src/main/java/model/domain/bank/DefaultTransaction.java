package model.domain.bank;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Class that implements the concept of a transaction.
 */
public class DefaultTransaction implements Transaction {

    private static final long serialVersionUID = -8676396152502823263L;

    private double amount;
    private Date issuingDate;
    private String description;
    private TransactionType type;

    /**
     * Constructor of a transaction.
     *
     * @param description the reason for the transaction.
     * @param amount      money amount to deposit or to withdraw.
     * @param type        type of the transaction (deposit or withdraw).
     */
    public DefaultTransaction(String description, double amount, TransactionType type) {
        this.issuingDate = Calendar.getInstance().getTime();
        this.type = type;
        this.setMoneyAmount(amount);
        this.setDescription(description);
    }

    /**
     * Method to get the identification number of an entity.
     *
     * @return the identification number.
     */
    @Override
    public long getId() {
        return this.issuingDate.getTime();
    }

    /**
     * Method to get the date of the transaction.
     *
     * @return the date of the tranaction.
     */
    @Override
    public Date getIssuingDate() {
        return this.issuingDate;
    }

    /**
     * Method to get the type of the transaction (positive or negative)
     *
     * @return the sign that represent the type of the transaction (+ or -).
     */
    @Override
    public TransactionType getType() {
        return this.type;
    }

    /**
     * Method to get the amount of the transaction.
     *
     * @return amount of the transaction
     */
    @Override
    public double getMoneyAmount() {
        return this.amount;
    }

    /**
     * Method to set the amount of the transaction.
     *
     * @param amount of the transaction.
     */
    @Override
    public void setMoneyAmount(double amount) {
        this.amount = amount;
    }

    /**
     * Method to get the reason of the transaction.
     *
     * @return the reason of the transaction.
     */
    @Override
    public String getDescription() {
        return this.description;
    }

    /**
     * Method to set the description of the transaction.
     *
     * @param description the reason of the transaction.
     */
    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        SimpleDateFormat issuingDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        return "Transaction [ issuingDate: " + issuingDateFormat.format(issuingDate) + ", type: " + type
                + ", amount: " + amount + "description: " + description + " ]";
    }
}
