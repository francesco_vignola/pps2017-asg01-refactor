package model.domain.account;

import java.util.Objects;

public class DefaultAccount implements Account {
    private final String username;
    private String password;

    public DefaultAccount(String username, String password) {
        this.username = username;
        this.setPassword(password);
    }

    /**
     * Method to get the username.
     *
     * @return the username.
     */
    @Override
    public String getUsername() {
        return this.username;
    }

    /**
     * Method to get the password.
     *
     * @return the password.
     */
    @Override
    public String getPassword() {
        return this.password;
    }

    /**
     * Method to set the password.
     *
     * @param password to set.
     */
    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DefaultAccount)) return false;
        DefaultAccount that = (DefaultAccount) o;
        return getUsername().equals(that.getUsername());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUsername());
    }
}
