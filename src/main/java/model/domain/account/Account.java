package model.domain.account;

public interface Account {
    /**
     * Method to get the username.
     *
     * @return the username.
     */
    String getUsername();

    /**
     * Method to get the password.
     *
     * @return the password.
     */
    String getPassword();

    /**
     * Method to set the password.
     *
     * @param password to set.
     */
    void setPassword(String password);
}
