package model.domain.holding_cell;

import java.util.Objects;

/**
 * Concrete class of an holding cell.
 */
public class DefaultHoldingCell implements HoldingCell {

    private static final long serialVersionUID = 9167940013424894676L;

    private long id;
    private int capacity;
    private HoldingCellLocation location;
    private int currentPrisoners = 0;

    /**
     * Constructor to build an holding cell.
     *
     * @param id       unique number within prison.
     * @param location location of the holding cell in the prison.
     * @param capacity maximum number of prisoners a cell can accommodate.
     */
    public DefaultHoldingCell(long id, HoldingCellLocation location, int capacity) {
        this.id = id;
        this.setCapacity(capacity);
        this.setLocation(location);
    }

    /**
     * Method to get the identification number of an entity.
     *
     * @return the identification number.
     */
    @Override
    public long getId() {
        return id;
    }

    /**
     * Method to get the location of the holding cell in the prison.
     *
     * @return location of the holding cell.
     */
    @Override
    public HoldingCellLocation getLocation() {
        return location;
    }

    /**
     * Method to set the location of the holding cell in the prison.
     *
     * @param location of the holding cell.
     */
    @Override
    public void setLocation(HoldingCellLocation location) {
        this.location = location;
    }

    @Override
    public HoldingCellType getType() {
        return this.location.equals(HoldingCellLocation.FIRST_LEVEL)
                ? HoldingCellType.INSULATION
                : HoldingCellType.SHARED;
    }

    /**
     * Method to get the maximum capacity of the holding cell.
     *
     * @return maximum capacity of the holding cell.
     */
    @Override
    public int getCapacity() {
        return capacity;
    }

    /**
     * Method to set the maximum capacity of the holding cell.
     *
     * @param capacity of the holding cell.
     */
    @Override
    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    /**
     * Method to obtain the current number of prisoners housed in the detention cell.
     *
     * @return the current number of prisoners
     */
    @Override
    public int getCurrentPrisoners() {
        return currentPrisoners;
    }

    /**
     * Method to set the current number of prisoner housed in the detention cell.
     *
     * @param currentPrisoners housed in the detention cell.
     */
    @Override
    public void setCurrentPrisoners(int currentPrisoners) {
        this.currentPrisoners = currentPrisoners;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DefaultHoldingCell)) return false;
        DefaultHoldingCell cell = (DefaultHoldingCell) o;
        return getId() == cell.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    @Override
    public String toString() {
        return "CellImpl [id=" + id + ", location=" + location + ", capacity=" + capacity
                + ", currentPrisoners=" + currentPrisoners + "]";
    }
}
