package model.domain.holding_cell;

import model.persistence.Identifiable;

/**
 * Model the holding cell of the prison.
 */
public interface HoldingCell extends Identifiable {

    /**
     * Method to get the location of the holding cell in the prison.
     * @return location of the holding cell.
     */
    HoldingCellLocation getLocation();

    /**
     * Method to set the location of the holding cell in the prison.
     * @param location of the holding cell.
     */
    void setLocation(HoldingCellLocation location);

    /**
     * Method to get the type of the holding cell.
     * @return the type of the holding cell.
     */
    HoldingCellType getType();

    /**
     * Method to get the maximum capacity of the holding cell.
     * @return maximum capacity of the holding cell.
     */
    int getCapacity();

    /**
     * Method to set the maximum capacity of the holding cell.
     * @param capacity of the holding cell.
     */
    void setCapacity(int capacity);

    /**
     * Method to obtain the current number of prisoners housed in the detention cell.
     * @return the current number of prisoners
     */
    int getCurrentPrisoners();

    /**
     * Method to set the current number of prisoner housed in the detention cell.
     * @param currentPrisoners housed in the detention cell.
     */
    void setCurrentPrisoners(int currentPrisoners);
}
