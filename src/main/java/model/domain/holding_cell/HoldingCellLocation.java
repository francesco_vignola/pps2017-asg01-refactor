package model.domain.holding_cell;

public enum HoldingCellLocation {

    FIRST_LEVEL("Underground floor"),
    SECOND_LEVEL("First floor"),
    THIRD_LEVEL("Second floor"),
    FOURTH_LEVEL("Third floor");

    private final String location;

    HoldingCellLocation(final String location) {
        this.location = location;
    }

    @Override
    public final String toString() {
        return this.location;
    }
}
