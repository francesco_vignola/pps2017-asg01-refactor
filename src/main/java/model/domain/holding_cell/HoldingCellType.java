package model.domain.holding_cell;

public enum HoldingCellType {
    INSULATION,
    SHARED;
}
