import controller.Implementations.LoginControllerImpl;
import model.domain.holding_cell.DefaultHoldingCell;
import model.domain.holding_cell.HoldingCellLocation;
import model.domain.person.DefaultPoliceman;
import model.domain.person.Policeman;
import view.Interfaces.LoginView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The main of the application.
 */
public final class Main {

    /**
     * Program main, this is the "root" of the application.
     *
     * @param args unused,ignore
     */
    public static void main(final String... args) {
        //creo cartella in cui mettere i dati da salvare
        String Dir = "res";
        new File(Dir).mkdir();
        //creo file per le guardie
        File fg = new File("res/GuardieUserPass.txt");
        //se il file non è stato inizializzato lo faccio ora
        if (fg.length() == 0) {
            try {
                initializeGuards(fg);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //leggo il file contenente le celle
        File f = new File("res/Celle.txt");
        //se il file non è ancora stato inizializzato lo faccio ora
        if (f.length() == 0) {
            try {
                initializeCells(f);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        //chiamo controller e view del login
        new LoginControllerImpl(new LoginView());
    }

    /**
     * metodo che inizializza le celle
     *
     * @param f file in cui creare le celle
     * @throws IOException
     */
    static void initializeCells(File f) throws IOException {

        List<DefaultHoldingCell> list = new ArrayList<>();
        DefaultHoldingCell c;
        for (int i = 0; i < 50; i++) {
            if (i < 20) {
                c = new DefaultHoldingCell(i, HoldingCellLocation.SECOND_LEVEL, 4);
            } else if (i < 40) {
                c = new DefaultHoldingCell(i, HoldingCellLocation.THIRD_LEVEL, 3);
            } else if (i < 45) {
                c = new DefaultHoldingCell(i, HoldingCellLocation.FOURTH_LEVEL, 4);
            } else {
                c = new DefaultHoldingCell(i, HoldingCellLocation.FIRST_LEVEL, 1);
            }
            list.add(c);
        }
        FileOutputStream fo = new FileOutputStream(f);
        ObjectOutputStream os = new ObjectOutputStream(fo);
        os.flush();
        fo.flush();
        for (DefaultHoldingCell c1 : list) {
            os.writeObject(c1);
        }
        os.close();
    }

    /**
     * metodo che inizializza le guardie
     *
     * @param fg file in cui creare le guardie
     * @throws IOException
     */
    static void initializeGuards(File fg) throws IOException {

        String pattern = "MM/dd/yyyy";
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        Date date = null;
        try {
            date = format.parse("01/01/1980");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        List<Policeman> list = new ArrayList<>();

        DefaultPoliceman.PolicemanBuilder builder = new DefaultPoliceman.PolicemanBuilder();
        builder.setId(1)
                .setRank(1)
                .setTelephone("0764568")
                .setBirthday(date)
                .setName("Oronzo")
                .setSurname("Cantani")
                .setPassword("ciao01");

        list.add(builder.build());

        builder.setId(2)
                .setRank(2)
                .setTelephone("456789")
                .setBirthday(date)
                .setName("Emile")
                .setSurname("Heskey")
                .setPassword("asdasd");
        list.add(builder.build());


        builder.setId(3)
                .setRank(3)
                .setTelephone("0764568")
                .setBirthday(date)
                .setName("Gennaro")
                .setSurname("Alfieri")
                .setPassword("qwerty");
        list.add(builder.build());

        FileOutputStream fo = new FileOutputStream(fg);
        ObjectOutputStream os = new ObjectOutputStream(fo);
        os.flush();
        fo.flush();
        for (Policeman g : list) {
            os.writeObject(g);
        }
        os.close();
    }

}
