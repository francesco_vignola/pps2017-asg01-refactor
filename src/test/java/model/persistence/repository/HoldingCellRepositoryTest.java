package model.persistence.repository;

import javafx.util.Pair;
import model.domain.holding_cell.DefaultHoldingCell;
import model.domain.holding_cell.HoldingCellLocation;
import model.persistence.criteria.HoldingCellByCapacity;
import model.persistence.criteria.HoldingCellById;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.stream.Stream;

public class HoldingCellRepositoryTest {

    private static Repository<DefaultHoldingCell> repository = new CellRepository();

    @Before
    public void setUp() {
        String path = Paths.get(System.getProperty("user.dir"), "src", "res", "fileTest.txt").toString();
        try (ObjectOutputStream output = new ObjectOutputStream(FileUtils.openOutputStream(new File(path)))) {
            for(int id = 0; id < 50; id++) {
                Pair<HoldingCellLocation, Integer> floor = id < 20
                        ? new Pair<>(HoldingCellLocation.SECOND_LEVEL, 4)
                        : id < 40
                        ? new Pair<>(HoldingCellLocation.THIRD_LEVEL, 3)
                        : id < 49
                        ? new Pair<>(HoldingCellLocation.FOURTH_LEVEL, 4)
                        : new Pair<>(HoldingCellLocation.FIRST_LEVEL, 1);

                DefaultHoldingCell cell = new DefaultHoldingCell(id, floor.getKey(), floor.getValue());
                repository.add(cell);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @After
    public void tearDown() {
        repository.clear();
    }

    @Test
    public void findById() {
        Optional<DefaultHoldingCell> cell = repository.find(new HoldingCellById(30));
        Assert.assertEquals(30, cell.get().getId());
    }

    @Test
    public void filterWithCriteria() {
        Stream<DefaultHoldingCell> cell = repository.filter(new HoldingCellByCapacity(4));
        Assert.assertEquals(29, cell.count());
    }

    @Test
    public void findByCapacity() {
        Optional<DefaultHoldingCell> cell = repository.find(new HoldingCellByCapacity(1));
        Assert.assertEquals(49,cell.get().getId());
    }

    @Test
    public void addElement() {
        DefaultHoldingCell cell = new DefaultHoldingCell(61, HoldingCellLocation.FOURTH_LEVEL, 4);
        repository.add(cell);
        Optional<DefaultHoldingCell> expectedCell = repository.find(new HoldingCellById(61));
        Assert.assertEquals(expectedCell, Optional.of(cell));
    }

    @Test
    public void removeElement() {
        DefaultHoldingCell cell = new DefaultHoldingCell(1, HoldingCellLocation.FIRST_LEVEL, 1);
        repository.remove(cell);
        Optional<DefaultHoldingCell> expectedCell = repository.find(new HoldingCellById(1));
        Assert.assertEquals(Optional.empty(), expectedCell);
    }

}